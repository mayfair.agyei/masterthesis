import numpy as np
np.set_printoptions(precision=3) # to make the outputs easier to read
import pylops
import pyproximal
import matplotlib.pyplot as plt

# TASK: 
## Check for convergence for the task 1 and task 2 (visualize) - DONE
# Check using the L2 norm (visualize) - DONE
# Check for convergence without the NOISE (visualize)

#EXERCISE 1
# notes = we only get convergence with small values of tau eg. 0.01, 0.001 (Plot to show it)
# notes = Using the L2 (i.e using line 85 we only get convergence with tau=0.001 (very small) but requires very few iteration numbers for convergence)
# notes = with the noise (uncomment line 63) it seems to converge with smaller number of iterations and also smaller values of tau?????


# EXERCISE 2
# notes = faster convergence especially with smaller values of tau. 
# notes = larger values of alpha causes a smooth error plot
class proxF(pyproximal.ProxOperator):
    """
    text
    """
    def __init__(self):
        super().__init__()
       
    # Here we ensure that we have a positive semi-definite matrix  
    def __call__(self,A):
        # to allow matrix input in matrix or vector shape
        n = int(len(A.reshape(-1))**(1/2)) # convert it so i can get a square matrix
        A.shape = (n,n) # this will give an error, if the matrix is not square (WE define the shape of the matrix we want)
        s, u = np.linalg.eigh(A) #eigen values of the matrix
        for i in s:
            if i < 0:
                return np.inf
        return 0
    
    
    def prox(self, A, tau = 1.):
        # to allow matrix input in matrix or vector shape
        orig_shape = A.shape
        n = int(len(A.reshape(-1))**(1/2))
        A.shape = (n,n) # this will give an error, if the matrix is not square

        # 
        s,u = np.linalg.eigh(A)
        s_new = np.zeros(s.shape)
        for i,si in enumerate(s):
            if si < 0:
                s_new[i] = 0
            else:
                s_new[i] = s[i]
        return (u@np.diag(s_new)@u.T).reshape(orig_shape)
 
    
# define the matrix T, which defines our linear operator
TT = np.array([(theta+1)**(-abs(l-i)-abs(l-j)) \
                for l in range(3) for theta in range(4) \
                for i in range(3) for j in range(3)]).reshape((3,4,3,3))
T = np.reshape(TT,(12,9)) # this way we can use the matrix as operator
# writing the operator T as pylops operator
# n = np.random.normal(0, 10, T.shape) # adding noise  makes it very bad
T = T # + n

# WE just convert the matrix into pylops for ease of use
Top = pylops.MatrixMult(T) #PYLOPS linear operator toolbox inspired by matlab

# initialize some marix that we will try to recover and apply T
# choose a very simple positive definite matrix, here identity
rho = np.eye(3).reshape(-1)
g = T@rho # = Top(rho)
# print(rho.reshape((3,-1)))
# print(g.reshape((3,-1)))



# Define the two functions R and S (or f and g)
#------------------------
# penalty term
R = proxF() # initialize the class and we get our Matrix 

# other term
# S = pyproximal.L1(g=g) # We define the Trho in the L1 norm
S = pyproximal.L2() # changing it to L2
# initial values
x0 = np.ones((3,3))
x0.shape = -1

error = []

iter = 20
# Run Primal-dual hybrid gradient (PDHG) or Chambolle-Pock algorithm
cp_sol = pyproximal.optimization.primaldual.PrimalDual(R, S, Top, tau=0.001, mu=1., theta=0.1, x0=x0, niter=iter, show=False, gfirst=False,callback=lambda x:error.append(np.linalg.norm(x-rho)))
    
tau_values = [0.5, 0.1, 0.01, 0.001]
ncol = 2
nrow = len(tau_values) // ncol + (len(tau_values) % ncol > 0)

fig, axs = plt.subplots(nrows=nrow, ncols=ncol, figsize=(15, 12))
fig.suptitle('Task A: Comparing the effects of tau on convergence', fontsize=12)

z = np.linspace(0, iter, iter)
for val, ax in zip(tau_values,  axs.ravel()):
    error = []
    cp_sol = pyproximal.optimization.primaldual.PrimalDual(R, S, Top, tau=val, mu=1., theta=0.1, x0=x0, niter=iter, show=False, gfirst=False,callback=lambda x:error.append(np.linalg.norm(x-rho)))
    ax.semilogy(z, error) #loglog  or semilogy or plot (normally for convergence plot you ensure both the x and y axis are in log coordinate)
    ax.set_xlabel('iterations')
    ax.set_ylabel('Error')
    ax.set_title(r'tau: %.3f' %val)
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(labels, loc='upper right')
plt.show()

####################### SECTION 2 ########################################

class proxG(pyproximal.ProxOperator):
    """
    text
    """
    def __init__(self,alpha):
        super().__init__()
        self.alpha = alpha
        self.F = proxF() # This is in L1
        self.L2 = pyproximal.proximal.L2(sigma = self.alpha) # This is in L2
        
    def __call__(self,A):
        return self.F(A) + self.L2(A)  
        # to allow matrix input in matrix or vector shape
        n = int(len(A.reshape(-1))**(1/2))
        A.shape = (n,n) # this will give an error, if the matrix is not square
        #
        s,u = np.linalg.eigh(A)
        for i in s:
            if i < 0:
                return np.inf
        return self.alpha * np.linalg.norm(A)**2
    
    def prox(self, A, tau = 1.):
        return self.F.prox(self.L2.prox(A, tau),tau)
    
    
rho = np.eye(3).reshape(-1)
g = T@rho # = Top(rho)
# print(rho.reshape((3,-1)))
# print(g.reshape((3,-1)))


# Define the two functions R and S (or f and g)
#------------------------
# penalty term
R = proxG(alpha=1)
# other term
S = pyproximal.L1(g = g)

# initial values
x0 = np.zeros((3,3))
x0.shape = -1

error = []
iter  = 20
# Run Primal-dual hybrid gradient (PDHG) or Chambolle-Pock algorithm
cp_sol = pyproximal.optimization.primaldual.PrimalDual(R, S, Top,
                                        tau=0.01,mu=0.1, theta=1., 
                                        x0=x0, niter=iter,
                                        show=False, gfirst=False,
                                        callback=lambda x:error.append(np.linalg.norm(x-rho)))

tau_values = [0.5, 0.1, 0.01, 0.001]
z = np.linspace(0, iter, iter)
ncol = 2
nrow = len(tau_values) // ncol + (len(tau_values) % ncol > 0)

fig, axs = plt.subplots(nrows=nrow, ncols=ncol, figsize=(15, 12))
fig.suptitle('Task B: Comparing the effects of tau on convergence', fontsize=12)

for val, ax in zip(tau_values, axs.ravel()):
    error = []
    cp_sol = pyproximal.optimization.primaldual.PrimalDual(R, S, Top,tau=val,mu=0.1, theta=1., 
                                        x0=x0, niter=iter,
                                        show=False, gfirst=False,
                                        callback=lambda x:error.append(np.linalg.norm(x-rho)))    
    ax.semilogy(z, error)
    ax.set_xlabel('iterations')
    ax.set_ylabel('Error')
    ax.set_title(r'tau: %.3f' %val)
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(labels, loc='upper right')
plt.show()


alpha_values = [1, 5, 10, 100]
z = np.linspace(0, iter, iter)
ncol = 2
nrow = len(alpha_values) // ncol + (len(alpha_values) % ncol > 0)

fig, axs = plt.subplots(nrows=nrow, ncols=ncol, figsize=(15, 12))
fig.suptitle('Task B: Comparing the effects of alpha on convergence', fontsize=12)

for val, ax in zip(alpha_values, axs.ravel()):
    R = proxG(alpha=val)
    error = []
    cp_sol = pyproximal.optimization.primaldual.PrimalDual(R, S, Top, tau=0.01,mu=0.1, theta=1., 
                                        x0=x0, niter=iter,
                                        show=False, gfirst=False,
                                        callback=lambda x:error.append(np.linalg.norm(x-rho)))    
    ax.semilogy(z, error)
    ax.set_xlabel('iterations')
    ax.set_ylabel('Error')
    ax.set_title(r'Section2 : alpha %.0f' %val)
plt.show()
